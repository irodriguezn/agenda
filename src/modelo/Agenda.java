/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author nacho
 */
public class Agenda {
    private Contacto agenda []= new Contacto [50];
    private int numElementos=0;

    public Contacto[] getAgenda() {
        return agenda;
    }

    public void setAgenda(Contacto[] agenda) {
        this.agenda = agenda;
    }
    
    public boolean añadirContacto(Contacto c) {
        int e=Integer.parseInt(c.getTelefono());
        int i=0;
        boolean salir=false;
        boolean esIgual=false;
        while(i<numElementos && ! salir ){
            if((Integer.parseInt(agenda[i].getTelefono())>e)){
             salir=true;   
            } else {
               if ((Integer.parseInt(agenda[i].getTelefono())==e)) {
                   esIgual=true;
                   salir=true;
               }
            }
            i++;
        }
        if (!esIgual) {        
            if (!salir) {// Insertamos al final
                agenda[numElementos]=c;
            }else{
                desplazarDerecha(i-1);
                agenda[i-1]=c;
            }
            numElementos++;// Incrementamos el número de elementos:
        }
        return !esIgual;
    }
    
    // Devuelve el contacto que coincida con el nombre pasado como parámetro y
    // si no lo encuentra devuelve null
    public Contacto obtenerContactoApellido1(String apellido1) {
        Contacto c=null;
        for (int i = 0; i < numElementos; i++) {
            if  (agenda[i].getApellido1().equals(apellido1)) {
                c=agenda[i];
                break;
            }
        }
        return c;
    }
    
    // Devuelve el contacto que coincida con el nombre pasado como parámetro y
    // si no lo encuentra devuelve null
    public Contacto obtenerContactoTelefono(String telefono) {
        Contacto c=null;
        for (int i = 0; i < numElementos; i++) {
            if  (agenda[i].getTelefono().equals(telefono)) {
                c=agenda[i];
                break;
            }
        }
        return c;
    }    

    @Override
    public String toString() {
        String cad;
        cad="Agenda\n";
        cad+="------\n";
        for (int i = 0; i < numElementos; i++) {
            cad+=agenda[i].toString()+"\n";
        }
        return cad;
    }
    
    public void desplazarDerecha(int e){
        for(int i=numElementos;i>e;i--){
            agenda[i]=agenda[i-1];
        }
    }
}
