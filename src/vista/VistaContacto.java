/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.Contacto;

/**
 *
 * @author nacho
 */
public class VistaContacto {
    public static Contacto leeContacto() {
        String nombre=Vista.leeCadena("Introduzca el nombre: ");
        String apellido1=Vista.leeCadena("Introduzca el primer apellido: ");
        String apellido2=Vista.leeCadena("Introduzca el segundo apellido: ");
        String telefono=Vista.leeCadena("Introduzca el teléfono: ");
        Contacto c=new Contacto(nombre, apellido1, apellido2, telefono);
        return c;
    }
}
