/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class Vista {

    public static String leeCadena(String texto) {
        String cadena="";
        Scanner sc = new Scanner(System.in);
        System.out.print(texto);
        cadena=sc.nextLine();
        return cadena;
    }
    
    public static void muestraMensaje(String texto) {
        System.out.println(texto);
    }
    
    public static void pausa() {
        System.out.println("");
        leeCadena("Pulse una tecla para continuar");
    }
    
    public static String menuPrincipal() {
        System.out.println("AGENDA");
        System.out.println("------");
        System.out.println("1 - Crear Contacto");
        System.out.println("2 - Buscar Contacto por Apellido");
        System.out.println("3 - Buscar Contacto por Teléfono");
        System.out.println("4 - Ver Contactos");
        System.out.println("0 - Salir");
        return leeCadena("Introduzca una opción: ");
    }
    
}
