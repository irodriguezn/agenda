/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.*;
import vista.*;


/**
 *
 * @author nacho
 */
public class Controlador {

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Agenda a = new Agenda();
        String op="";
        
        
        //Contacto c1=new Contacto("Pepe", "Rodriguez", "Navarro", "366 54 23");
        boolean salir=false;
        while (!salir) {
            op= Vista.menuPrincipal();
            if (manejaMenu(op, a).equals("0")) {
                salir=true;
            }
        }
        
        //Contacto c2=new Contacto("José", "Pinto", "Gutiérrez", "344 34 25");
        //a.añadirContacto(c2);
        //System.out.println(a.toString());
    }
    
    public static String manejaMenu(String op, Agenda a) {
        switch (op) {
            case "1": // Agregar Contacto
                if (!añadirContacto(a)) {
                    Vista.muestraMensaje("El contacto ya existe!!");
                }
                Vista.pausa();
                break;
            case "2": // Buscar por nombre
                buscarApellido1(a);
                Vista.pausa();
                break;
            case "3": // Buscar por teléfono
                buscarTelefono(a);
                Vista.pausa();
                break;
            case "4": // Mostrar agenda
                System.out.println(a.toString());
                Vista.pausa();
                break;            
            case "0": // Salir
        }
        return op;
    }
    
    public static boolean añadirContacto(Agenda a) {
        Contacto c=VistaContacto.leeContacto();
        return a.añadirContacto(c);
    }
    
    public static Contacto buscarApellido1(Agenda a) {
        String apellido1="";
        Vista.muestraMensaje("Búsqueda por Apellido");
        Vista.muestraMensaje("-------------------");
        apellido1=Vista.leeCadena("Introduce el primer apellido:");
        Contacto c=a.obtenerContactoApellido1(apellido1);
        if (c!=null) {
            System.out.println(c.toString());
        } else {
            Vista.muestraMensaje("El contacto no existe!!");
        }
        return c;
    }
    
    public static Contacto buscarTelefono(Agenda a) {
        String telefono="";
        Vista.muestraMensaje("Búsqueda por Teléfono");
        Vista.muestraMensaje("-------------------");
        telefono=Vista.leeCadena("Introduce el teléfono:");
        Contacto c=a.obtenerContactoApellido1(telefono);
        if (c!=null) {
            System.out.println(c.toString());
        } else {
            Vista.muestraMensaje("El contacto no existe!!");
        }
        return c;
    }
}
